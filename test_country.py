import unittest
from check_number import check_number
from find_country import find_country

class TestStringMethods(unittest.TestCase):
    def test_NoPlus(self):
        self.assertFalse(check_number('375333535135'))

    def test_findCountry(self):
        self.assertEqual(find_country(375), 'Belarus')

    def test_whiteSpace(self):
        self.assertFalse(check_number('+375 33'))

    def test_onlyDigits(self):
        self.assertFalse(check_number('+375sfasg84'))

    def test_goodNumber(self):
        self.assertTrue(check_number('+375333535135'))

    def test_unknownCountry(self):
        self.assertEqual(find_country(000), 'Unknown country')

if __name__ == '__main__':
    unittest.main()