from country import CODE_COUNTRY

def find_country(code):
    country = CODE_COUNTRY.get(code)
    if country != None:
        return country
    else:
        return "Unknown country"

